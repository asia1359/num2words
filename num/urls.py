from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.conversion_form),
    url(r'^word/$', views.conversion_result)
]
from django.db import models


class Number (models.Model):
    number = models.IntegerField()
    word = models.CharField(max_length=200)

    def __str__(self):
        return self.word

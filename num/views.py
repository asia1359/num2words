from django.shortcuts import render
from .models import Number
from django.template import loader
from django.http import HttpResponse
from num2words import num2words


def conversion_form(request):
    return render(request, 'form.html')


def conversion_result(request):
    if 'number' in request.GET:
        num = '%s' % request.GET['number']
        word = num2words(num, lang='pl')
        return render(request, 'word.html', {'word': word})

    else:
        return HttpResponse('Nie podano liczby!!')

